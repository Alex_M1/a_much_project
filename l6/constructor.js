// function Auto(model, price) {
//     this.model = model;
//     this.price = price;
// }

// Auto.prototype.getModel = function() {
//     return this.model;
// }

// Auto.prototype.getInfo = function() {
//     return `Модель: ${this.model}, цена: ${this.price}`;
// }

class Auto {
    constructor(model, price) {
            this.model = model;
            this.price = price;
    }

    getInfo() {
    return `Модель: ${this.model}, цена: ${this.price}`;
    }
}
    
const auto1 = new Auto('Lamborgini', 1000000);

console.log(auto1.getInfo());