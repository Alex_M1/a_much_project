class Secretary {

    greeting() {
        console.log('Добрый день')
    }

    question() {
        console.log('Вы по какому вопросу?')
    }

    waiting() {
        console.log('Ожидайте на линии')
    }

    switching() {
        console.log('Перевожу на менеджера')
    }

    dayOver() {
        console.log('Наш рабочий день закончен')
    }

    recall() {
        console.log('Перезвоните в рабочее время')
    }

    bye() {
        console.log('До свидания!')
    }
}

class SecretaryFacade
{
    constructor(secretary) {
        this.secretary = secretary
    }

    ready() {
        this.secretary.greeting()
        this.secretary.question()
        this.secretary.waiting()
        this.secretary.switching()
    }

    offline() {
        this.secretary.dayOver()
        this.secretary.recall()
        this.secretary.bye()
    }
}

const secretary = new SecretaryFacade(new Secretary())
secretary.ready() 
secretary.offline() 